<?php
/**
 * @file
 * teacher_role.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function teacher_role_user_default_roles() {
  $roles = array();

  // Exported role: teacher.
  $roles['teacher'] = array(
    'name' => 'teacher',
    'weight' => 11,
  );

  return $roles;
}
